import socket, sys, base64, os.path

def _mysend(socket, msg, MSGLEN):
    totalsent = 0
    while totalsent < MSGLEN:
        sent = socket.send(msg[totalsent:])
        if sent == 0:
            raise RuntimeError("socket connection broken")
        totalsent = totalsent + sent

def _myreceive(socket, MSGLEN):
    chunks = []
    bytes_recd = 0
    while bytes_recd < MSGLEN:
        chunk = socket.recv(min(MSGLEN - bytes_recd, 2048))
        if chunk == '':
            raise RuntimeError("socket connection broken")
        chunks.append(chunk)
        bytes_recd = bytes_recd + len(chunk)
    return b''.join(chunks)

def mysend(socket, msg):
    _mysend(socket, len(msg).to_bytes(8, 'big', signed=False), 8)
    _mysend(socket, msg, len(msg))

def myreceive(socket):
    return _myreceive(socket, int.from_bytes(_myreceive(socket, 8), 'big', signed=False))

def read_file(filename):
    with open(filename, 'rb') as f:
        return str(base64.b64encode(f.read()), 'utf-8')

def write_file(filename, data):
    if os.path.exists(filename):
        raise FileExistsError("cannot overwrite existing file")
    with open(filename, 'wb') as f:
        f.write(base64.b64decode(bytes(data, 'utf-8')))