import os
import socket
import json
import base64
from utils import *

srv_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
srv_sock.bind((sys.argv[1], int(sys.argv[2])))
srv_sock.listen(5)
while True:
    client_sock, client_addr = srv_sock.accept()
    request = json.loads(myreceive(client_sock).decode())
    response = dict()
    if request['type'] == 'get':
        try:
            response['file_contents'] = read_file(request['file_name'])
            response['code'] = 'OK'
        except Exception as e:
            response['code'] = 'ERROR [SERVER] ' + str(e)
    elif request['type'] == 'put':
        try:
            write_file(request['file_name'], request['file_contents'])
            response['code'] = 'OK'
        except Exception as e:
            response['code'] = 'ERROR [SERVER] ' + str(e)
    elif request['type'] == 'list':
        response['list'] = os.listdir()
        response['code'] = 'OK'
    mysend(client_sock, json.dumps(response).encode())

srv_sock.shutdown(socket.SHUT_WR)
srv_sock.close()