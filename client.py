import socket
import sys
import json
from utils import *

request = dict()
if sys.argv[3] == 'put':
    try:
        request = { 'type': 'put', 'file_name': sys.argv[4], 'file_contents': read_file(sys.argv[4]) }
    except Exception as e:
        print("ERROR [CLIENT] " + str(e))
        sys.exit()
elif sys.argv[3] == 'get':
    request = { 'type': 'get', 'file_name': sys.argv[4] }
elif sys.argv[3] == 'list':
    request = { 'type': 'list' }

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try:
    sock.connect((sys.argv[1], int(sys.argv[2])))
except Exception as e:
    print("ERROR [CLIENT] " + str(e))
    sys.exit()
mysend(sock, json.dumps(request).encode())
response = json.loads(myreceive(sock).decode())
sock.shutdown(socket.SHUT_WR)
sock.close()

if response['code'] == 'OK':
    if sys.argv[3] == 'get':
        try:
            write_file(sys.argv[4], response['file_contents'])
        except Exception as e:
            print("ERROR [CLIENT] " + str(e))
            sys.exit()
    elif sys.argv[3] == 'list':
        print(response['list'])
print(response['code'])

